from pynput import keyboard
from pynput.keyboard import Key, Controller
import pyautogui
import time
from colorama import Fore, init
import random

timer = 5
loopamount = 2
#for Reseting Color Of the Line
init(autoreset=True)


keyboard = Controller()

#Script Slower Random Number Generator
#Exmaple : 1 - 60 Represents 1 Secs <-> 60 Secs or 1 Min
# Don't go below 100 for the base Line
def Ssn():
   ssn=random.randrange(1, 60)
   print(Fore.GREEN + 'Script Slow Down Number = ', ssn)
   return ssn


#Main
for i in range(loopamount):
 keyboard.press('w')
 time.sleep(timer)
 keyboard.release('w')
 keyboard.press(Key.space)
 time.sleep(Ssn())
print(Fore.GREEN + 'Stopped')




