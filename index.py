#Import module
import wmi
import os
from colorama import Fore, init
#import time
from subprocess import call
from pynput import keyboard

#Important things 
#Hotkey for Starting antiafk
COMB =[
     {keyboard.KeyCode(char='+')}

]
current = set()
#for Reseting Color Of the Line
init(autoreset=True)
# Initializing the wmi constructor
f = wmi.WMI()
PID = os.getpid()
flag = 0

# Iterating through all the running processes
for process in f.Win32_Process():
	if "ragemp_v.exe" == process.Name:
		print(Fore.GREEN + '[+] Application is Running | PID : ' , PID )
		flag = 1
		break

print("Press Num + to Start")
def execute():
    print("Starting")
    call(["python", "antiafk.py"])



def on_press(key):
    if any([key in COMBO for COMBO in COMB]):
        current.add(key)
        if any(all(k in current for k in COMBO) for COMBO in COMB):
            execute()

def on_release(key):
    if any([key in COMBO for COMBO in COMB]):
        current.remove(key)

with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
    listener.join()


if flag == 0:
	print(Fore.RED + '[-] Application is not Running Closing')

